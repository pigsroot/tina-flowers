package com.tina.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author Tina
 * @since 2020-10-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class Food implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 商品ID
     */
    @TableId(value="id",type= IdType.AUTO)
    private Integer id;

    /**
     * 商品名字
     */
    private String title;

    /**
     * 描述
     */
    private String foodDesc;

    /**
     * 商品封面图
     */
    private String cover;

    /**
     * 商品图
     */
    private String foodImg;

    /**
     * 原价
     */
    private BigDecimal originPrice;

    /**
     * 售价
     */
    private BigDecimal sellPrice;

    /**
     * 折扣
     */
    private BigDecimal discount;

    /**
     * 点赞
     */
    private Integer foodLike;

    /**
     * 限购数量
     */
    private Integer limitNum;

    /**
     * 规格选项
     */
    private String options;

    /**
     * 总的销量
     */
    private Integer totalSales;

    /**
     * 月销量
     */
    private Integer monthSales;

    /**
     * 好评率
     */
    private Float praiseRate;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 加入时间
     */
    private String addTime;

    /**
     * 编辑时间
     */
    private String editTime;

    /**
     * 排序
     */
    @TableField(exist = false)
    private String sortBy;


}
