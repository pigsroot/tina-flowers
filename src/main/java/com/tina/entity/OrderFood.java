package com.tina.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author Tina
 * @since 2020-10-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class OrderFood implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 订单主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 订单ID
     */
    private String orderId;

    /**
     * 商品ID
     */
    private Integer foodId;

    /**
     * 商品标题
     */
    private String title;

    /**
     * 商品封面
     */
    private String cover;

    /**
     * 原价
     */
    private BigDecimal originPrice;

    /**
     * 售价
     */
    private BigDecimal sellPrice;

    /**
     * 下单数量
     */
    private Integer number;

    /**
     * 订单状态（0成功，1待付款，2已取消）
     */
    private Integer status;

    /**
     * 新增时间
     */
    private String addTime;

    /**
     * 编辑
     */
    private String editTime;

}
