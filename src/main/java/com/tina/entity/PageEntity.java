package com.tina.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


/**
 * @Author tina
 * @Time 2020/10/28 下午5:17
 * @Version 1.0
 * @Content
 **/
@AllArgsConstructor
@Data
@NoArgsConstructor
public class PageEntity {

    /**
     * 数据的条数
     */
    private Long count;

    /**
     * 内容
     */
    private Object data;

}
