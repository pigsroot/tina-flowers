package com.tina.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tina.entity.Food;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author Tina
 * @since 2020-10-26
 */
public interface FoodMapper extends BaseMapper<Food> {

}
