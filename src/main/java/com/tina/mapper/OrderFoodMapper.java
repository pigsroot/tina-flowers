package com.tina.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tina.entity.OrderFood;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author Tina
 * @since 2020-10-26
 */
public interface OrderFoodMapper extends BaseMapper<OrderFood> {

}
