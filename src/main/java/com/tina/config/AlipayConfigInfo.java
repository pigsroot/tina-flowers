package com.tina.config;

import java.io.FileWriter;
import java.io.IOException;

/**
 * @Author tina
 * @Time 2020/10/26 上午9:00
 * @Version 1.0
 * @Content
 **/
public class AlipayConfigInfo {
    /**
     * 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
     */
    public static String app_id = "2021000116667012";

    /**
     * 商户私钥，您的PKCS8格式RSA2私钥
     */
    public static String merchant_private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCLbq/CtCbA4nn8CyC9YzvKuZ0o1ncXJ9X1n36Mzd9eVLK3i5Uw+jaKfOkW6+LBpL4k+ocHVjK0mnRLZkaOwgbk75aQ8Zv9kPqDpZdhyNY8vFxDY9oxehGAMV5o4GJs545pmPqykyftKWBjolrRL0gn8eo6kB6Dv/SJQrWoSWU5FtzFbnVqYTMEKYxTHPtEkNje+Z5Dj1CWRHyrcIaQ1le8p86/rEJ+9vzqMPOy5IoZGAAkLw+ucQVLl+q22ERKE7LtSZockHLV0pW4P9jO2DID7hqFb07nGyK8l41FlDFGJpq7SFjiXvvWlia3aCYb+PpZqprrcVSsiEmlzfZ5yZsRAgMBAAECggEAeg39i4oZ9nUKZWj0UvUADTkoauw6OankcOwaYnWBF9k6zNNCWSpe/iHUBP2edPaagFJSS0jYgCJItQwlragJUvSsL9tgOrpjoUQeXTnq2CXm+hL5mKALm9UbmOD7J+wXTTbeFd4dcEztFKq1pzIljtoEuRkZrSkmjdU4ocUF989RwWjDB0FHxkYsfXn2v3uBtzsnllYm+jd6Bw3ZOWESdDjaWJdZXA2tARTOey8ZI8fRbqp2vp4b43HxSJYV9Gt1pm//8j1jb5kO1cVyBaChstOU//pPUcZ4H8uTXEQaxmaJBSfivktW27rodquWgGoZJrb8PO2a+2LXFZCBm18zhQKBgQDA2jxsekojF1h4X01eZ8To7dYXjHqlOtfT1eFnHVeXJ1eQIWMcAZ4TILTXOHBYgykLrwr8537eISHw6YaQlRm3dHdGxhiwpy6PRsNFAhn8YWrF0bGdaE4o3qnos4uK3tNKVq7EA6xHTCQOpcrkoZY/93FTAMbjMnhWLnH+Fc2fjwKBgQC5FoktkbJjIJx/zeNTjHgKOMqI2toeGsR/5PNC0G5Tv/shwP/pAXay9+FvYlrK6GODxuDIrNZgi2x9dODdZXXP24Vsn/F2HNxNYcO5z0ZSpHddF+EFNoC3EIKgVh2puc5TU3uZeWgziegirFdbXlm+Kyhp2mfdlN0pvWkxATvLXwKBgQCh5JJk3iPuYWK7agWhAquifkYDbsJmgoc8JYga797f3P7814pDsHfqxsIJCjAFndWPIVqwBAXUoyvC+cclCIWWJAcdOp7vbOkBDf5Op/z4iS8A8Y+Q/EbguEsaNd8mrPfip/H+SrcNAzYhtbwlQiDHggl4DGbpwyyR7Ft1WQhMsQKBgC1Zwj7yJh13ziDal8rO7lLC241w4mTh9ifIN3q1i3Ll5Wi923aCL01mw0wBc3GoQ0n0nvtMrjJ1tx1+V+Kni5hkl2qoPgfMxoYHYGfUy8xQFBcxVKdsVVmFqBwVPatqpY1xNSXDw65tKf3k8HqowaCA/qQBKdX3NcGWcYHaZYZNAoGAbwgylXSmNLvXNgsOcSp62x7dJXULQXp14i/vkTkITpIccym5x7eg7sfthcKULIho5gsyTpDYwaQD7TnfAXtXNN7/VGSi3NwH4xFhUjwagZRC7maYXcKHD9T9p2/mHO8oBefmq7Br22NMMNA4G3FmPbhE6yCS1+jrrbITVwXTWfI=";

    /**
     * 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm
     * 对应APPID下的支付宝公钥。
     */
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAp+MF5j7aMl955wCaC5v43rUndgQhsLBClnqgheEf2gxyusnw2Z+y0UoD2ZwHHc73yAn57ti7xRS/rpUyeMCpKEA2yshWhF3RjHgJGZzaRnMX+/dRy+lBDES4HtucugyVkgMHmgo4gd2Inw5k7sxkqy/SlDwsMQ3f06YqbCZxYVoXxkOZ8WG2Dovn7+GiRaADxqcFnpV7GKtG0hbNdU4/7KBD8/pbdS61MI3MsK+TR/TltNf4YW8/RdJ5o87Pc59AbLegSihROJNA6qgZ3DMf7f6ijhsjv+VHaBhmUL6BcXpPIet+RxordnXH38ov23krrfp6zUgCNTiLWMf5mLMl5wIDAQAB";

    /**
     * 服务器异步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
     * 返回的时候此页面不会返回到用户页面，只会执行你写到控制器里的地址
     */
    public static String notify_url = "http://5mfcrs.natappfree.cc/notifyPayResult";

    /**
     * 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
     * 此页面是同步返回用户页面，也就是用户支付后看到的页面，上面的notify_url是异步返回商家操作，谢谢
     * 要是看不懂就找度娘，或者多读几遍，或者去看支付宝第三方接口API，不看API直接拿去就用，遇坑不怪别人,要使用外网能访问的ip,建议使用花生壳,内网穿透
     */
    public static String return_url = "http://5mfcrs.natappfree.cc/shop.html";

    /**
     * 签名方式
     */
    public static String sign_type = "RSA2";

    /**
     * 字符编码
     */
    public static String charset = "utf-8";

    /**
     * 支付宝网关
     */
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";


}
