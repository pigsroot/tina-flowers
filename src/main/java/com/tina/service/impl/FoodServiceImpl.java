package com.tina.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tina.entity.Food;
import com.tina.mapper.FoodMapper;
import com.tina.service.IFoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author Tina
 * @since 2020-10-26
 */
@Service
public class FoodServiceImpl extends ServiceImpl<FoodMapper, Food> implements IFoodService {

    @Autowired
    private FoodMapper foodMapper;

    @Override
    public Integer insertFood(Food food) {
        return foodMapper.insert(food);
    }

    @Override
    public Integer editFood(Food food) {
        return foodMapper.updateById(food);
    }

    @Override
    public List<Food> selectFood(Food food) {
        QueryWrapper<Food> queryWrapper = new QueryWrapper<>();
        if (food.getSortBy() != null) {
            if (food.getSortBy().equals("newDate")) {
                queryWrapper.orderByDesc("add_time");
            }
            if (food.getSortBy().equals("priceAsc")) {
                queryWrapper.orderByAsc("sell_price");
            }
            if (food.getSortBy().equals("priceDesc")) {
                queryWrapper.orderByDesc("sell_price");
            }

        }
        return foodMapper.selectList(queryWrapper);
    }
}
