package com.tina.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tina.entity.Food;
import com.tina.entity.OrderFood;
import com.tina.mapper.OrderFoodMapper;
import com.tina.service.IOrderFoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author Tina
 * @since 2020-10-26
 */
@Service
public class OrderFoodServiceImpl extends ServiceImpl<OrderFoodMapper, OrderFood> implements IOrderFoodService {

    @Autowired
    private OrderFoodMapper orderFoodMapper;

    @Override
    public Integer insertOrderFood(OrderFood orderFood) {
        return orderFoodMapper.insert(orderFood);
    }

    @Override
    public Integer editOrderFood(OrderFood orderFood) {
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.eq("order_id", orderFood.getOrderId());
        return orderFoodMapper.update(orderFood, updateWrapper);
    }

    @Override
    public List<OrderFood> selectOrderFood(OrderFood orderFood) {
        QueryWrapper<OrderFood> queryWrapper = new QueryWrapper<>();
        return orderFoodMapper.selectList(queryWrapper);
    }

    @Override
    public OrderFood selectOrderFoodById(String orderId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("order_id", orderId);
        return orderFoodMapper.selectOne(queryWrapper);
    }
}
