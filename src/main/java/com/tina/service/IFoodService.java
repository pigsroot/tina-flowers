package com.tina.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tina.entity.Food;

import java.util.List;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author Tina
 * @since 2020-10-26
 */
public interface IFoodService extends IService<Food> {

    /**
     * 新增商品
     *
     * @param food
     * @return
     */
    Integer insertFood(Food food);

    /**
     * 编辑商品
     *
     * @param food
     * @return
     */
    Integer editFood(Food food);

    /**
     * 查询商品
     *
     * @param food
     * @return
     */
    List<Food> selectFood(Food food);

}
