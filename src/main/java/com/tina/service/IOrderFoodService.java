package com.tina.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tina.entity.Food;
import com.tina.entity.OrderFood;

import java.util.List;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author Tina
 * @since 2020-10-26
 */
public interface IOrderFoodService extends IService<OrderFood> {
    /**
     * 新增订单
     *
     * @param orderFood
     * @return
     */
    Integer insertOrderFood(OrderFood orderFood);

    /**
     * 编辑订单
     *
     * @param orderFood
     * @return
     */
    Integer editOrderFood(OrderFood orderFood);

    /**
     * 查询订单
     *
     * @param orderFood
     * @return
     */
    List<OrderFood> selectOrderFood(OrderFood orderFood);

    /**
     * 通过订单ID查询订单
     *
     * @param orderId
     * @return
     */
    OrderFood selectOrderFoodById(String orderId);
}
