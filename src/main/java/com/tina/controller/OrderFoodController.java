package com.tina.controller;


import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.tina.config.AlipayConfigInfo;
import com.tina.entity.AlipayBean;
import com.tina.entity.OrderFood;
import com.tina.service.IOrderFoodService;
import com.tina.util.AliPayUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author Tina
 * @since 2020-10-26
 */
@RestController
@RequestMapping("/orderFood/")
public class OrderFoodController {
    private Logger logger = LoggerFactory.getLogger(OrderFoodController.class);
    @Autowired
    private IOrderFoodService orderFoodService;

    @Resource
    private AliPayUtil aliPayUtil;

    /**
     * 新增订单
     *
     * @param orderFood
     * @param httpResponse
     * @throws IOException
     * @throws AlipayApiException
     */
    @PostMapping("add")
    public void insertOrderFood(@RequestBody OrderFood orderFood, HttpServletResponse httpResponse) throws IOException, AlipayApiException {
        logger.info("orderFood->{}", orderFood);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        orderFood.setAddTime(simpleDateFormat.format(new Date()));
        orderFood.setOrderId(String.valueOf(System.currentTimeMillis()));
        orderFoodService.insertOrderFood(orderFood);
        //2. 调用支付宝
        AlipayBean alipayBean = new AlipayBean();
        alipayBean.setOut_trade_no(orderFood.getOrderId());
        alipayBean.setSubject(orderFood.getTitle());
        alipayBean.setTotal_amount(String.valueOf(orderFood.getSellPrice()));
        String pay = aliPayUtil.pay(alipayBean);
        logger.info("pay->{}", pay);
        httpResponse.setContentType("text/html;charset="+AlipayConfigInfo.charset);
        httpResponse.getWriter().write(pay);
        httpResponse.getWriter().flush();
        httpResponse.getWriter().close();
    }

}
