package com.tina.controller;

import com.alipay.api.internal.util.AlipaySignature;
import com.tina.config.AlipayConfigInfo;

import com.tina.entity.OrderFood;
import com.tina.service.IOrderFoodService;
import com.tina.util.OrderEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;

import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author tina
 * @Time 2020/10/26 上午9:01
 * @Version 1.0
 * @Content
 **/
@RestController
@RequestMapping("/")
public class AlipayControlller {

    @Autowired
    private IOrderFoodService orderFoodService;

    private Logger logger = LoggerFactory.getLogger(AlipayControlller.class);

    /**
     * 快捷支付调用支付宝支付接口
     *
     * @param model，id，payables，
     * @return Object
     */
    @RequestMapping("alipaySum")
    public Object alipayIumpSum(Model model, String WIDout_trade_no, String WIDsubject, String WIDtotal_amount, String WIDbody,
                                HttpServletResponse response)
            throws Exception {
        System.out.println("请求进来" + WIDtotal_amount);
        String payables = WIDout_trade_no;
        // 订单名称，必填(必须是数字)
        String subject = WIDsubject;
        // 付款金额，必填
        String total_fee = WIDtotal_amount;
        // 商品描述，可空
        String body = WIDbody;
        // 获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfigInfo.gatewayUrl, AlipayConfigInfo.app_id,
                AlipayConfigInfo.merchant_private_key, "json", AlipayConfigInfo.charset,
                AlipayConfigInfo.alipay_public_key, AlipayConfigInfo.sign_type);
        // 设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(AlipayConfigInfo.return_url);
        alipayRequest.setNotifyUrl(AlipayConfigInfo.notify_url);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        // 商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = sdf.format(new Date());
        // 付款金额，必填
        String total_amount = payables.replace(",", "");
        alipayRequest.setBizContent("{\"out_trade_no\":\"" + out_trade_no + "\"," + "\"total_amount\":\"" + total_amount
                + "\"," + "\"subject\":\"" + subject + "\"," + "\"body\":\"" + body + "\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
        // 请求
        String result = alipayClient.pageExecute(alipayRequest).getBody();
        System.out.println(result);
        //EnvUtils.setEnv(EnvUtils.EnvEnum.SANDBOX);

        response.setContentType("text/html; charset=gbk");
        PrintWriter out = response.getWriter();
        out.print(result);
        System.out.println("out->{}" + out);
        return null;
    }

    /**
     * 支付成功的回调接口
     *
     * @return
     */
    @Transactional
    @RequestMapping("notifyPayResult")
    public String notifyPayResult(HttpServletRequest request) {
        logger.info("<-----进入支付宝回调----->");
        // 1.从支付宝回调的request域中取值放到map中
        Map<String, String[]> requestParams = request.getParameterMap();

        Map<String, String> params = new HashMap();
        for (String name : requestParams.keySet()) {
            String[] values = requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            params.put(name, valueStr);
        }
        //2.封装必须参数
        // 商户订单号 新接口 交易状态去掉了
        String outTradeNo = params.get("out_trade_no");

        logger.info("outTradeNo:{}", outTradeNo);

        //3.签名验证(对支付宝返回的数据验证，确定是支付宝返回的)
        boolean signVerified = false;
        try {
            //3.1调用SDK验证签名
            signVerified = AlipaySignature.rsaCheckV1(params, AlipayConfigInfo.alipay_public_key, AlipayConfigInfo.charset, AlipayConfigInfo.sign_type);

        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("验签结果:{}", signVerified);

        //4.对验签进行处理
        if (signVerified) {
            //验签通过
            //只处理支付成功的订单: 修改交易表状态,支付成功

            //根据订单号查找订单,防止多次回调的问题
            OrderFood orderFood = orderFoodService.selectOrderFoodById(outTradeNo);
            if (orderFood != null && orderFood.getStatus() == OrderEnum.ORDER_STATUS_NOT_PAY.getStatus()) {
                //修改订单状态
                orderFood.setStatus(OrderEnum.ORDER_STATUS_PAID.getStatus());
                orderFoodService.editOrderFood(orderFood);
            }
            return "success";

        } else {
            //验签不通过
            System.err.println("-------------------->验签失败");
            return "failure";
        }
    }
}
