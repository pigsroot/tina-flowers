package com.tina.controller;


import com.alibaba.fastjson.JSONObject;
import com.tina.entity.Food;
import com.tina.entity.PageEntity;
import com.tina.service.IFoodService;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品表 前端控制器
 * </p>
 *
 * @author Tina
 * @since 2020-10-26
 */
@RestController
@RequestMapping("/food/")
public class FoodController {
    @Autowired
    private RestHighLevelClient restHighLevelClient;
    @Autowired
    private IFoodService foodService;

    private Logger logger = LoggerFactory.getLogger(FoodController.class);



    @GetMapping("list")
    public PageEntity list(Food food, @RequestParam("page") Integer page, @RequestParam("limit") Integer limit) throws IOException {


        SearchRequest request = new SearchRequest("tina_index");
        SearchSourceBuilder builder = new SearchSourceBuilder();

        if (food.getSortBy() != null) {
            if (food.getSortBy().equals("newDate")) {
            }
            if (food.getSortBy().equals("priceAsc")) {

            }
            if (food.getSortBy().equals("priceDesc")) {

            }

        }

        // 分页
        builder.from(page - 1);
        builder.size(limit);
        // 条件查询


        //builder.sort("addTime", SortOrder.DESC);
        request.source(builder);
        SearchResponse search = restHighLevelClient.search(request, RequestOptions.DEFAULT);
        long totalCount = search.getHits().getTotalHits().value;


        // 接收结果
        ArrayList<Map<String, Object>> arrayList = new ArrayList<Map<String, Object>>();

        for (SearchHit hit : search.getHits().getHits()) {
            arrayList.add(hit.getSourceAsMap());
        }

        return new PageEntity(totalCount, arrayList);
    }


    /**
     * 批量插入数据到搜索引擎中
     *
     * @param food
     * @return
     * @throws IOException
     */
    @GetMapping("bulk")
    public BulkResponse bulk(Food food) throws IOException {
        logger.info("food->{}", food);

        BulkRequest bulkRequest = new BulkRequest();
        List<Food> foods = foodService.selectFood(food);
        for (Food food1 : foods) {
            logger.info("food1->{}", food1);
        }
        for (int i = 0; i < foods.size(); i++) {
            bulkRequest.add(
                    new IndexRequest("tina_index")
                            .id("" + (i + 1)).source(JSONObject.toJSONString(foods.get(i)), XContentType.JSON)
            );
        }

        // 成功 false 失败true
        BulkResponse bulk = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
        System.out.println(bulk.hasFailures());
        return bulk;
    }


    /**
     * 搜索引擎
     */
    @GetMapping("searchRed/{title}")
    public ArrayList<Map<String, Object>> searchRed(@PathVariable String title) throws IOException {

        // 条件搜索
        SearchRequest request = new SearchRequest("tina_index");
        SearchSourceBuilder builder = new SearchSourceBuilder();

        // 分页
        builder.from(0);
        builder.size(10);

        // 高亮字段
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.field("title");
        // 关闭多个高亮
        highlightBuilder.requireFieldMatch(true);
        highlightBuilder.preTags("<span style='color:red'>");
        highlightBuilder.postTags("</span>");
        builder.highlighter(highlightBuilder);

        // 条件查询
        MatchQueryBuilder termQueryBuilder = QueryBuilders.matchQuery("title", title);
        builder.query(termQueryBuilder);

        // 发送请求
        request.source(builder);
        SearchResponse search = restHighLevelClient.search(request, RequestOptions.DEFAULT);

        // 接收结果
        ArrayList<Map<String, Object>> arrayList = new ArrayList<Map<String, Object>>();

        for (SearchHit hit : search.getHits().getHits()) {
           // arrayList.add(hit.getSourceAsMap());

            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
            HighlightField name = highlightFields.get("title");

            // 获取结果
            Map<String, Object> sourceAsMap = hit.getSourceAsMap();

            // 解析高亮字段
            if (name != null) {
                Text[] fragments = name.fragments();
                String newName = "";
                for (Text fragment : fragments) {
                    newName += fragment;
                }
                sourceAsMap.put("title", newName);
            }
            // 高亮字段替换原来的内容
            arrayList.add(sourceAsMap);
        }
        for (Map<String, Object> map : arrayList) {
            System.out.println("map-> "+map);
        }
        return arrayList;
    }

}
